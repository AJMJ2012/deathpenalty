using System.IO;
using Terraria.IO;
using Terraria.DataStructures;
using Microsoft.Xna.Framework;
using System;
using Terraria;
using Terraria.ID;
using Terraria.ModLoader;

namespace DeathPenalty {
	public class MPlayer : ModPlayer {
		public override void OnEnterWorld(Player player) {
			if (player != Main.player[Main.myPlayer] || Main.netMode != 1) { return; }
			DALib.Logger.DebugLog("Requesting Config from Server", Config.modPrefix);
			ModPacket netMessage = mod.GetPacket();
			netMessage.Write(Config.modPrefix);
			netMessage.Write((byte)Requests.RequestData);
			netMessage.Write((int)player.whoAmI);
			netMessage.Send();
		}

		public override void Kill(double damage, int hitDirection, bool pvp, PlayerDeathReason damageSource) {
			bool penalized = false;
			if (player.statLifeMax >= Config.Server.MinHealth + Config.Server.HealthLoss) {
				player.statLifeMax -= Config.Server.HealthLoss;
				player.statLifeMax2 -= Config.Server.HealthLoss;
				CombatText.NewText(new Microsoft.Xna.Framework.Rectangle((int)player.Center.X, (int)player.Center.Y, player.width, player.height), Colors.RarityRed, ("-" + Config.Server.HealthLoss + "HP"), true, true);
				DALib.Logger.DebugLog("Reduced " + player.name + "'s health by " + Config.Server.HealthLoss, Config.modPrefix);
				penalized = true;
			}
			if (player.statManaMax >= Config.Server.MinMana + Config.Server.ManaLoss) {
				player.statManaMax -= Config.Server.ManaLoss;
				player.statManaMax2 -= Config.Server.ManaLoss;
				CombatText.NewText(new Microsoft.Xna.Framework.Rectangle((int)player.Center.X, (int)player.Center.Y, player.width, player.height), Colors.RarityRed, ("-" + Config.Server.ManaLoss + "MP"), true, true);
				DALib.Logger.DebugLog("Reduced " + player.name + "'s mana by " + Config.Server.ManaLoss, Config.modPrefix);
				penalized = true;
			}
			if (penalized) {
				Main.NewText("You have been penalized for dying.", Colors.RarityRed);
			}
		}

		public override void OnRespawn(Player player) {
			player.AddBuff(163, 30, false);
			if (Config.Server.DarknessDebuffLength > 0) {
				player.AddBuff(22, Config.Server.DarknessDebuffLength * 30, false);
				DALib.Logger.DebugLog("Debuffed " + player.name + " with Darkness for " + (Config.Server.DarknessDebuffLength) + " seconds", Config.modPrefix);
			}
			if (Config.Server.SlowDefuffLength > 0) {
				player.AddBuff(32, Config.Server.SlowDefuffLength * 30, false);
				DALib.Logger.DebugLog("Debuffed " + player.name + " with Slow for " + (Config.Server.SlowDefuffLength) + " seconds", Config.modPrefix);
			}
			if (Config.Server.WeakDefuffLength > 0) {
				player.AddBuff(33, Config.Server.WeakDefuffLength * 30, false);
				DALib.Logger.DebugLog("Debuffed " + player.name + " with Weak for " + (Config.Server.WeakDefuffLength) + " seconds", Config.modPrefix);
			}
		}

		public override void UpdateDead() {
			if (player.whoAmI == Main.myPlayer) {
				player.headcovered = true;
			}
		}
	}
}