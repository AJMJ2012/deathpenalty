using Microsoft.Xna.Framework;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System;
using Terraria.ID;
using Terraria.IO;
using Terraria.ModLoader;
using Terraria;

namespace DeathPenalty {
	public enum Requests : byte {
		RequestData,
		ReceiveConfig
	}

	public class DeathPenalty : Mod {
		public DeathPenalty() {
			Properties = new ModProperties() {
				Autoload = true
			};
		}

		bool LoadedFKTModSettings = false;
		public override void Load() {
			Config.ReadConfig();
			if (Main.netMode == 2) { return; }
			LoadedFKTModSettings = ModLoader.GetMod("FKTModSettings") != null;
			if (LoadedFKTModSettings) {
				try { LoadModSettings(); }
				catch (Exception e) {
					DALib.Logger.ErrorLog("Unable to Load Mod Settings", Config.modPrefix);
					DALib.Logger.ErrorLog(e, Config.modPrefix);
				}
			}
		}
		
		public override void AddRecipes() {
			ModRecipe modRecipe = new ModRecipe(this);

			modRecipe = new ModRecipe(this);
			modRecipe.AddIngredient(109, 2);	// Mana Crystal
			modRecipe.AddIngredient(3544, 1);	// Super Healing Potion
			modRecipe.AddTile(26);				// Demon Altar
			modRecipe.SetResult(29, 2);			// Life Crystal
			modRecipe.AddRecipe();

			modRecipe = new ModRecipe(this);
			modRecipe.AddIngredient(109, 1);	// Mana Crystal
			modRecipe.AddIngredient(499, 1);	// Greater Healing Potion
			modRecipe.AddTile(26);				// Demon Altar
			modRecipe.SetResult(29, 1);			// Life Crystal
			modRecipe.AddRecipe();

			modRecipe = new ModRecipe(this);
			modRecipe.AddIngredient(109, 1);	// Mana Crystal
			modRecipe.AddIngredient(188, 2);	// Healing Potion
			modRecipe.AddTile(26);				// Demon Altar
			modRecipe.SetResult(29, 1);			// Life Crystal
			modRecipe.AddRecipe();

			modRecipe = new ModRecipe(this);
			modRecipe.AddIngredient(109, 1);	// Mana Crystal
			modRecipe.AddIngredient(28, 4);		// Lesser Healing Potion
			modRecipe.AddTile(26);				// Demon Altar
			modRecipe.SetResult(29, 1);			// Life Crystal
			modRecipe.AddRecipe();
			/*
			modRecipe = new ModRecipe(this);
			modRecipe.AddIngredient(109, 1);	// Mana Crystal
			modRecipe.AddIngredient(5, 8);		// Mushroom
			modRecipe.AddIngredient(23, 16);	// Gel
			modRecipe.AddTile(26);				// Demon Altar
			modRecipe.SetResult(29, 1);			// Life Crystal
			modRecipe.AddRecipe();
			*/
		}

		public override void PostUpdateInput() {
			if (LoadedFKTModSettings && !Main.gameMenu && Main.netMode != 2) {
				if (DALib.DALib.tick % 60 == 0) {
					try {
						List<Type> Types = new List<Type>{ typeof(Config.Global) };
						if (Main.netMode != 2) { Types.Add(typeof(Config.Client)); }
						if (Main.netMode != 1) { Types.Add(typeof(Config.Server)); }
						string OldConfig = null;
						foreach (Type type in Types) { OldConfig += JsonConvert.SerializeObject((type.GetFields(BindingFlags.Static | BindingFlags.Public).Select(field => field.GetValue(type)))); }
						UpdateModSettings();
						string NewConfig = null;
						foreach (Type type in Types) { NewConfig += JsonConvert.SerializeObject((type.GetFields(BindingFlags.Static | BindingFlags.Public).Select(field => field.GetValue(type)))); }
						if (OldConfig != NewConfig) { Config.SaveConfig(); }
					}
					catch (Exception e) {
						DALib.Logger.ErrorLog("Unable to compare config data", Config.modPrefix);
						DALib.Logger.ErrorLog(e, Config.modPrefix);
					}
				}
			}
		}

		private void LoadModSettings() {
			FKTModSettings.ModSetting setting = FKTModSettings.ModSettingsAPI.CreateModSettingConfig(this);
			setting.EnableAutoConfig();
			setting.AddComment("- Server Settings", 1f);
			setting.AddComment("Health Penalty", 0.8f);
			setting.AddInt("MinHealth", "Minimum Health", 20, 400, true);
			setting.AddInt("HealthLoss", "Health Lost", 0, 100, true);

			setting.AddComment("Mana Penalty", 0.8f);
			setting.AddInt("MinMana", "Minimum Mana", 0, 200, true);
			setting.AddInt("ManaLoss", "Mana Lost", 0, 100, true);

			setting.AddComment("Debuff Penalty", 0.8f);
			setting.AddInt("DarknessDebuffLength", "Darkness Penalty Time", 0, 600, true);
			setting.AddInt("SlowDefuffLength", "Slow Penalty Time", 0, 600, true);
			setting.AddInt("WeakDefuffLength", "Weak Penalty Time", 0, 600, true);
		}

		private void UpdateModSettings() {
			FKTModSettings.ModSetting setting;
			if (FKTModSettings.ModSettingsAPI.TryGetModSetting(this, out setting)) {
				setting.Get("MinHealth", ref Config.Server.MinHealth);
				setting.Get("HealthLoss", ref Config.Server.HealthLoss);
				setting.Get("MinMana", ref Config.Server.MinMana);
				setting.Get("ManaLoss", ref Config.Server.ManaLoss);
				setting.Get("DarknessDebuffLength", ref Config.Server.DarknessDebuffLength);
				setting.Get("SlowDefuffLength", ref Config.Server.SlowDefuffLength);
				setting.Get("WeakDefuffLength", ref Config.Server.WeakDefuffLength);
			}
		}

		public override void HandlePacket(BinaryReader reader, int whoAmI) {
			if (reader.ReadString() != this.Name) { return; }
			Requests request = (Requests)reader.ReadByte();
			if (Main.netMode == 2) {
				if (request == Requests.RequestData) {
					Config.SendConfig(this, whoAmI);
				}
			}
			else {
				if (request == Requests.ReceiveConfig) {
					DALib.Logger.DebugLog("Received Config from Server", Config.modPrefix);
					Config.Server.MinHealth = reader.ReadInt32();
					Config.Server.HealthLoss = reader.ReadInt32();
					Config.Server.MinMana = reader.ReadInt32();
					Config.Server.ManaLoss = reader.ReadInt32();
					Config.Server.DarknessDebuffLength = reader.ReadInt32();
					Config.Server.SlowDefuffLength = reader.ReadInt32();
					Config.Server.WeakDefuffLength = reader.ReadInt32();
				}
			}
		}
	}

	public static class Config {
		public static class Global {}
		public static class Client {}
		public static class Server {
			public static int MinHealth = 100;
			public static int HealthLoss = 20;
			public static int MinMana = 20;
			public static int ManaLoss = 20;
			public static int DarknessDebuffLength = 10;
			public static int SlowDefuffLength = 5;
			public static int WeakDefuffLength = 20;
		}

		public static string modName = "DeathPenalty";
		public static string modPrefix = "DP";
		private static Preferences Configuration = new Preferences(Path.Combine(Main.SavePath, "Mod Configs/" + modName + ".json"));
		public static void ReadConfig() {
			if (Configuration.Load()) {
				if (Main.netMode != 1) {
					Configuration.Get("MinHealth", ref Server.MinHealth);
					Configuration.Get("HealthLoss", ref Server.HealthLoss);
					Configuration.Get("MinMana", ref Server.MinMana);
					Configuration.Get("ManaLoss", ref Server.ManaLoss);
					Configuration.Get("DarknessDebuffLength", ref Server.DarknessDebuffLength);
					Configuration.Get("SlowDefuffLength", ref Server.SlowDefuffLength);
					Configuration.Get("WeakDefuffLength", ref Server.WeakDefuffLength);
				}
				DALib.Logger.DebugLog("Config Loaded", Config.modPrefix);
			}
			else {
				DALib.Logger.DebugLog("Creating Config", Config.modPrefix);
			}
			SaveConfig();
		}

		public static void ClampConfig() {
			Server.MinHealth = (int)MathHelper.Clamp(Server.MinHealth, 20, 400);
			Server.HealthLoss = (int)MathHelper.Clamp(Server.HealthLoss, 0, 100);
			Server.MinMana = (int)MathHelper.Clamp(Server.MinMana, 0, 200);
			Server.ManaLoss = (int)MathHelper.Clamp(Server.ManaLoss, 0, 100);
			Server.DarknessDebuffLength = (int)MathHelper.Clamp(Server.DarknessDebuffLength, 0, 600);
			Server.SlowDefuffLength = (int)MathHelper.Clamp(Server.SlowDefuffLength, 0, 600);
			Server.WeakDefuffLength = (int)MathHelper.Clamp(Server.WeakDefuffLength, 0, 600);
		}

		public static void SaveConfig() {
			ClampConfig();
			if (Main.netMode != 1) { Configuration.Clear(); }
			if (Main.netMode != 1) {
				Configuration.Put("MinHealth", Server.MinHealth);
				Configuration.Put("HealthLoss", Server.HealthLoss);
				Configuration.Put("MinMana", Server.MinMana);
				Configuration.Put("ManaLoss", Server.ManaLoss);
				Configuration.Put("DarknessDebuffLength", Server.DarknessDebuffLength);
				Configuration.Put("SlowDefuffLength", Server.SlowDefuffLength);
				Configuration.Put("WeakDefuffLength", Server.WeakDefuffLength);
			}
			Configuration.Save();
			DALib.Logger.DebugLog("Config Saved", Config.modPrefix);
		}

		public static void SendConfig(Mod mod, int whoAmI = -1) {
			ModPacket netMessage = mod.GetPacket();
			netMessage.Write(Config.modPrefix);
			netMessage.Write((byte)Requests.ReceiveConfig);
			netMessage.Write(Server.MinHealth);
			netMessage.Write(Server.HealthLoss);
			netMessage.Write(Server.MinMana);
			netMessage.Write(Server.ManaLoss);
			netMessage.Write(Server.DarknessDebuffLength);
			netMessage.Write(Server.SlowDefuffLength);
			netMessage.Write(Server.WeakDefuffLength);
			if (whoAmI >= 0) { netMessage.Send(whoAmI); } else { netMessage.Send(); }
			DALib.Logger.DebugLog("Config sent to " + (whoAmI >= 0 ? Main.player[whoAmI].name : "all players"), Config.modPrefix);
		}
	}

	public class Commands : ModCommand {
		public override CommandType Type {
			get { return CommandType.Chat | CommandType.Server | CommandType.Console; }
		}
		public override string Command {
			get { return Config.modPrefix.ToLower(); }
		}
		public override string Description {
			get { return Config.modName; }
		}
		public override string Usage {
			get { return Command + " reload"; }
		}
		public override void Action(CommandCaller caller, string input, string[] args) {
			switch (args[0].ToLower()) {
				case "reload":
					DALib.Logger.Log("Config Reloaded", Config.modPrefix);
					Config.ReadConfig();
					if (Main.netMode == 2) {
						Config.SendConfig(mod);
					}
					return;
			}
		}
	}
}